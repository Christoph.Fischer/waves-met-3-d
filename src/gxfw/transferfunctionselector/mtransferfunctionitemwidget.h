/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MTRANSFERFUNCTIONITEMWIDGET_H
#define MTRANSFERFUNCTIONITEMWIDGET_H

#include <QWidget>

namespace Ui
{
class MTransferFunctionItemWidget;
}

namespace Met3D
{

/**
 * Widget for showing a transfer function colourmap preview and name in the MTransferFunctionSelectionDialog.
 * @see MTransferFunctionSelectionDialog.
 */
class MTransferFunctionItemWidget : public QWidget
{
Q_OBJECT

public:
    explicit MTransferFunctionItemWidget(QWidget *parent = nullptr);

    ~MTransferFunctionItemWidget() override;

    /**
     * Set colourmap data for this widget.
     * @param colourmapName Name of the colourmap
     * @param previewImage 1D Image representing the colourmap
     */
    void setData(const QString& colourmapName, const QImage& previewImage);

private:
    Ui::MTransferFunctionItemWidget *ui;

    QString name;
};

}

#endif // MTRANSFERFUNCTIONITEMWIDGET_H