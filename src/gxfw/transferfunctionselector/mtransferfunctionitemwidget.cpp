/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mtransferfunctionitemwidget.h"
#include "ui_mtransferfunctionitemwidget.h"

namespace Met3D
{

MTransferFunctionItemWidget::MTransferFunctionItemWidget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::MTransferFunctionItemWidget)
{
    ui->setupUi(this);
}


MTransferFunctionItemWidget::~MTransferFunctionItemWidget()
{
    delete ui;
}


void MTransferFunctionItemWidget::setData(const QString& colourmapName, const QImage& previewImage)
{
    this->name = colourmapName;

    ui->tfName->setText(colourmapName);
    QPixmap p = QPixmap::fromImage(previewImage);

    // Nearest neighbour scaling
    ui->tfImage->setPixmap(p.scaled(ui->tfImage->width(), p.height(), Qt::IgnoreAspectRatio, Qt::FastTransformation));

    // Linear interpolated scaling
    //ui->tfImage->setPixmap(p);
}

}