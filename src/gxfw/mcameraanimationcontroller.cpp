/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mcameraanimationcontroller.h"

// standard library imports

// related third party imports
#include <QVector3D>

// local application imports
#include "gxfw/msceneviewglwidget.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MCameraAnimationController::MCameraAnimationController()
        : animationState(nullptr)
{
}


MCameraAnimationController::~MCameraAnimationController()
{}


void MCameraAnimationController::initAnimation(MSceneViewGLWidget *view,
                                               MCameraSequence *sequence)
{
    AnimationState *state = new AnimationState(view, sequence);
    state->currentDistance = 0;
    state->currentKey = 0;
    state->currentTime = 0;

    isActive = true;
    isAnimationFinished = false;

    updateTotalPathLength(state);

    animationState = state;
}


bool MCameraAnimationController::advanceCurrentAnimation(long deltaTime)
{
    bool advanceTime = false; // Change to true if we moved over a key that
    // advances time animation.

    auto state = animationState;
    MCameraSequence *s = state->sequence;

    if (s->keys.length() == 0) // If we don't have any keys, don't animate
    {
        advanceTime = false;
        isAnimationFinished = true;
        return advanceTime;
    }

    int key1Index = state->currentKey;
    // If our current key is out of bounds, use the last key. Only happens when
    // editing sequence while it animates.
    if (key1Index >= s->keys.length())
        key1Index = s->keys.length() - 1;
    if (key1Index < 0)
        key1Index = 0;

    MSequenceKey *key1 = s->keys[key1Index];

    // Get the rotation and location for the camera at key1, used later.
    QVector3D camPos = key1->position;
    QVector3D camRot = key1->rotation;

    if (s->keys.length() > 1) // Only advance along path if it even exists.
    {
        bool keyPairFound = false;
        int key2Index;
        MSequenceKey *key2;

        // Calculate delta as distance the camera moved in deltaTime time.
        double totalPathLength = state->totalPathLength;
        long runTime = s->sequenceRuntime * 1000;
        double delta = totalPathLength / runTime * deltaTime;

        while (!keyPairFound) // As long as we don't have a key pair we can transition between, search for one.
        {
            if (s->keys.length() > key1Index + 1) // We do have a next key
            {
                key2Index = key1Index + 1;
                key2 = s->keys[key2Index];
            }
            else if (s->loopSequence) // We don't have a next key, so we choose
                // the first key
            {
                key2Index = 0;
                key2 = s->keys[0];
            }
            else // We don't have any keys left, stay at current key
            {
                key2 = key1;
                keyPairFound = true;
                isAnimationFinished = true;
                break;
            }

            /*
             * Key 1 teleports to key 2. Don't advance further than key 2, wait
             * for next frame to do that
             */
            if (key1->transition == MCameraTransition::TELEPORT)
            {
                state->currentKey = key2Index;
                state->currentDistance = 0;

                advanceTime |= key2->advanceTimestep;

                keyPairFound = true;
                break;
            }

            double keyDist = distanceBetweenAnimationKeys(key1, key2);
            double finalDist = state->currentDistance + delta;

            if (finalDist >= keyDist) // We move past key2, set key2 as key1 and
                // search for new pair.
            {
                // Set current distance to 0, as delta gets added back in next
                // loop
                state->currentDistance = 0;
                // Set current key to key2
                state->currentKey = key2Index;
                // Update delta to be remaining delta past key2
                delta = finalDist - keyDist;

                key1Index = key2Index;
                key1 = key2;

                // Advance time ONCE if needed
                advanceTime |= key1->advanceTimestep;

                continue;
            }
            else // We don't move past key2, so we have a pair.
            {
                state->currentDistance = finalDist;
                keyPairFound = true;
                break;
            }
        }

        // Pair found, now update camera position and rotation
        // Teleport transition, just set position and rotation to key2 position
        // and rotation.
        if (key1->transition == MCameraTransition::TELEPORT)
        {
            camPos = key2->position;
            camRot = key2->rotation;
        }
        else if (key1 == key2) // Same key, no movement.
        {
            camPos = key1->position;
            camRot = key1->rotation;
        }
        else // Linear interpolate between key1 and key2.
        {
            // Total distance between both keys.
            double keyDist = distanceBetweenAnimationKeys(key1, key2);

            float alpha = state->currentDistance / keyDist;

            camPos = lerpPosition(key1->position, key2->position, alpha);
            camRot = lerpRotations(key1->rotation, key2->rotation, alpha);
        }
    }
    else
    {
        isAnimationFinished = true;
    }
    MCamera *cam = state->view->getCamera();

    cam->setOrigin(camPos);
    cam->setRotation(camRot.x(), camRot.y(), camRot.z());

    state->view->updateCamera();

    return advanceTime;
}


void MCameraAnimationController::stopAnimation()
{
    isActive = false;

    if (animationState == nullptr)
        return;

    delete animationState;

    animationState = nullptr;
}


void MCameraAnimationController::updateTotalPathLength(AnimationState *state) const
{
    MCameraSequence *s = state->sequence;

    for (int i = 0; i < s->keys.length(); i++)
    {
        if (s->keys.length() <= i + 1)
            return;

        MSequenceKey *key1 = s->keys[i];
        MSequenceKey *key2 = s->keys[i + 1];

        state->totalPathLength += distanceBetweenAnimationKeys(key1, key2);
    }

    if (s->loopSequence && s->keys.length() > 1)
    {
        state->totalPathLength += distanceBetweenAnimationKeys(
                s->keys[0], s->keys[s->keys.length() - 1]);
    }
}


double
MCameraAnimationController::distanceBetweenAnimationKeys(MSequenceKey *key1,
                                                         MSequenceKey *key2) const
{
    switch (key1->transition)
    {
    case MCameraTransition::TELEPORT:
        return 0;
    case MCameraTransition::LINTERP:
        return key1->position.distanceToPoint(key2->position); // LERP
    }

    return 0;
}


QVector3D MCameraAnimationController::lerpPosition(const QVector3D pos1,
                                                   const QVector3D pos2,
                                                   const float alpha) const
{
    return (1 - alpha) * pos1 + alpha * pos2;
}


QVector3D MCameraAnimationController::lerpRotations(const QVector3D rot1,
                                                    const QVector3D rot2,
                                                    const float alpha) const
{
    QVector3D delta = rot2 - rot1;

    if (fabs(delta.x()) > 180.0)
    {
        if (delta.x() < 0)
        {
            delta.setX(360.0 + delta.x());
        }
        else
        {
            delta.setX(-360.0 + delta.x());
        }
    }

    if (fabs(delta.y()) > 180.0)
    {
        if (delta.y() < 0)
        {
            delta.setY(360.0 + delta.y());
        }
        else
        {
            delta.setY(-360.0 + delta.y());
        }
    }

    if (fabs(delta.z()) > 180.0)
    {
        if (delta.z() < 0)
        {
            delta.setZ(360.0 + delta.z());
        }
        else
        {
            delta.setZ(-360.0 + delta.z());
        }
    }

    QVector3D res = rot1 + delta * alpha;

    return res;
}

} // namespace Met3D
