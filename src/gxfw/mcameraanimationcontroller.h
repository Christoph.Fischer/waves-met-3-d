/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MCAMERAANIMATIONCONTROLLER_H
#define MCAMERAANIMATIONCONTROLLER_H

// standard library imports

// related third party imports
#include <QList>

// local application imports
#include "data/sequencer/mcamerasequence.h"

namespace Met3D
{

class MSceneViewGLWidget;

/**
  Struct specifying an animation state.
  This animation state is used by the MCameraAnimationController
  to know which scene is animated, what camera sequence is used,
  and where in the camera sequence it currently is.
 */
struct AnimationState
{

    /**
      The scene view which camera is currently being animated.
     */
    MSceneViewGLWidget *view;

    /**
      The camera sequence used to animate the camera with.
     */
    MCameraSequence *sequence;

    /**
      The elapsed time from the start of the animation in milliseconds.
     */
    long currentTime;

    /**
     The total path length in world units for the camera sequence.
     This is only the length of one animation loop.
     */
    double totalPathLength;

    /**
      The distance from the current key on the animation sequence.
      This is used to interpolate between the current key and the next key in the sequence.
      The current key is specified in @ref currentKey.
     */
    double currentDistance;

    /**
      The key that the animation sequence currently interpolates from.
      When animating the sequence, this key is always the key the animation sequence last "visited",
      meaning it moved over the key or started from this key.
     */
    int currentKey;


    AnimationState(MSceneViewGLWidget *sceneView, MCameraSequence *animSequence)
            : view(sceneView),
              sequence(animSequence),
              currentTime(0),
              totalPathLength(0),
              currentDistance(0),
              currentKey(0)
    {}
};

/**
  The MCameraAnimationController class implements an animation controller that uses MCameraSequences to animate the camera
  of a specific scene. It currently can only animate one sequence for one scene at a time.
 */
class MCameraAnimationController
{
public:
    MCameraAnimationController();

    ~MCameraAnimationController();

    /**
      Initialize a new animation for the scene view @param view.
      The sequence used for this animation is specified in @param sequence.
     */
    void initAnimation(MSceneViewGLWidget *view, MCameraSequence *sequence);

    /**
      Advances the currently running animation in time
      for @param deltaTime milliseconds. This will update
      the camera that is updated to the new location and rotation.
      @return True when we moved over a key that specifies an advance in the time
      animation.
     */
    bool advanceCurrentAnimation(long deltaTime);

    /**
      Stops the running animation and resets the controllers state.
     */
    void stopAnimation();


    /**
      @return Whether the controller is currently active and animating a sequence.
     */
    bool isRunning() const
    { return isActive; }


    /**
      @return Whether the animation this controller animates is finished.
     */
    bool isFinished() const
    { return isAnimationFinished; }


private:
    /**
      Calculates the total path length for the animation sequence saved in the animation state @param state.
      Only calculates the path length of the sequence for one loop.
      Updates the total path length in the animation state.
     */
    void updateTotalPathLength(AnimationState *state) const;

    /**
      Calculates the distance between @param key1 and @param key2 based on the transition
      type of @param key1. If the transition type is @enum MCameraTransition::TELEPORT, the
      distance will be 0.
     */
    double distanceBetweenAnimationKeys(MSequenceKey *key1, MSequenceKey *key2) const;

    /**
      Linear interpolate between Positions @param pos1 and @param pos2, where @param alpha
      specifies the interpolated location between both positions.
     */
    QVector3D lerpPosition(const QVector3D pos1, const QVector3D pos2, const float alpha) const;

    /**
      Linear interpolate between Rotations @param rot1 and @param rot2, where @param alpha
      specifies the interpolated rotation between both positions.
     */
    QVector3D lerpRotations(const QVector3D rot1, const QVector3D rot2, const float alpha) const;

private:
    /**
      The current animation state of this animation controller.
     */
    AnimationState *animationState;

    /**
      Whether the controller is active or not.
     */
    bool isActive;

    /**
      Whether the animation is finished or not.
     */
    bool isAnimationFinished;

};

}

#endif // MCAMERAANIMATIONCONTROLLER_H
