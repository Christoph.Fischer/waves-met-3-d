/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MCAMERASEQUENCER_H
#define MCAMERASEQUENCER_H

// standard library imports

// related third party imports
#include <QWidget>
#include <QFileDialog>

// local application imports
#include "data/sequencer/msequencertablemodel.h"

namespace Ui {
    class MCameraSequencer;
}

namespace Met3D
{

/**
  Class implementing GUI functionality for the camera sequencer widget.
 */
class MCameraSequencer : public QWidget
{
    Q_OBJECT

public:
    explicit MCameraSequencer(QWidget *parent = nullptr);
    ~MCameraSequencer();

private:
    /**
      Adds an empty camera sequence and registers it in @ref Met3D::MSystemManagerAndControl.
      Automatically selects that sequence as the current one being edited.
     */
    void addDefaultSequence();

    /**
      Check whether @param filename already exist
      and prompt the user whether to overwrite it or not.
     */
    void checkExistenceAndSave(QString filename);

private slots:
    /**
      Called from UI to open a camera sequence from file.
     */
    void openSequence();

    /**
      Called from UI to save a camera sequence to file.
     */
    void saveSequence();

    /**
      Called from UI to save a camera sequence to a new file.
     */
    void saveAsSequence();

    /**
      Called from UI to add a new, empty key to the sequence,
      either after the selected key or last if none is selected.
     */
    void addNewKey();

    /**
      Called from UI to add a new key to the sequence,
      either after the selected key or last if none is selected.
      The keys' camera position and rotation values are taken from the camera
      that was last interacted with.
     */
    void addNewKeyFromCamera();

    /**
      Called from UI to set the position and rotation values
      of the selected key to the position and rotation of the camera
      that was last interacted with.
     */
    void setKeyFromCamera();

    /**
      Called from UI to view the selected key
      through the camera in the last interacted scene view.
      This places the camera at the position and rotation specified
      in the key.
     */
    void viewKeyInCamera();

    /**
      Called from UI to remove the selected key from the sequence.
     */
    void removeKey();

    /**
      Called from UI to remove the sequence that is currently being edited.
     */
    void removeSequence();

    /**
      Called from UI when the loop checkbox' state is updated to
      @param state.
      Updates whether the sequence that is currently being edited loops or not.
     */
    void loopChecked(int state);

    /**
      Called from UI when the sequence that is currently being edited is changed
      in the sequence combobox. The @param index specifies the index of the new sequence.
     */
    void sequenceChanged(int index);

    /**
      Called from UI when the sequence runtime is changed in the corresponding textbox.
      @param text is the runtime and needs to be converted to an integer.
     */
    void runtimeChanged(QString text);

    /**
     * Called from UI when the currently selected key should be moved up.
     */
    void moveKeyUp();

    /**
     * Called from UI when the currently selected key should be moved down.
     */
    void moveKeyDown();

    /**
      Called when a new sequence was registered in @ref Met3D::MSystemManagerAndControl.
      Used to update the combobox where the user can select the edited sequence from.
     */
    void onSequenceRegistered();

    /**
      Called when all sequences are removed on session change.
      Used to clear the sequence combobox and key table.
     */
    void onSequencesCleared();

    /**
      Called when the sequence at index @param index was removed
      from @ref Met3D::MSystemManagerAndControl.
      Used to update the combobox where the user can select the edited sequence from.
     */
    void onSequenceRemoved(int index);

    /**
      Called when the edited sequences name was changed in the sequence combobox.
      Used to update the name of the edited sequence.
      Also notify @ref Met3D::MSystemManagerAndControl of the new name, so it can emit
      a signal for other classes to know about the new name.
     */
    void onRenameSequence(QString newName);

    /**
      Called when a sequence was renamed in @ref Met3D::MSystemManagerAndControl.
      Used to update the combobox where the user can select the edited sequence from.
     */
    void onSequenceRenamed(int index, QString name);

    /**
      Called from UI to create a new sequence.
     */
    void onCreateCameraSequence();

private:
    Ui::MCameraSequencer *ui;

    /**
      The table model to display the currently edited sequences keys in.
      Also stores a reference to the sequence itself.
     */
    MSequencerTableModel *sequenceModel;
};

}

#endif // MCAMERASEQUENCER_H
