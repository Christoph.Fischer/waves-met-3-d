/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "msequencertablemodel.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSequencerTableModel::MSequencerTableModel(QObject *parent)
        : QAbstractTableModel(parent),
          sequence(nullptr)
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QVariant MSequencerTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::TextAlignmentRole)
    {
        if (orientation == Qt::Horizontal)
            return QVariant(int(Qt::AlignLeft | Qt::AlignVCenter));

        return QVariant(int(Qt::AlignRight | Qt::AlignVCenter));
    }

    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    // Horizontal header: Return the names of the columns.
    if (orientation == Qt::Horizontal)
    {
        switch (section)
        {
        case LON:
            return QVariant("Lon");
        case LAT:
            return QVariant("Lat");
        case Z:
            return QVariant("Z");
        case PITCH:
            return QVariant("Pitch");
        case YAW:
            return QVariant("Yaw");
        case ROLL:
            return QVariant("Roll");
        case TELEPORT_TRANSITION:
            return QVariant("Teleport to next key?");
        case NEXT_TIMESTEP:
            return QVariant("Increment time?");
        }
    }

    // Vertical header: Return the number of the row
    return QVariant(section);
}


int MSequencerTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    if (sequence == nullptr) return 0;

    return sequence->keys.count();
}


int MSequencerTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return COLUMN_COUNT;
}


bool MSequencerTableModel::insertRows(int position, int rows, const QModelIndex &index,
                                      QList<MSequenceKey *> *keys = nullptr)
{
    Q_UNUSED(index);

    beginInsertRows(QModelIndex(), position, position + rows - 1);

    for (int r = 0; r < rows; r++)
    {
        MSequenceKey *key;
        if (keys == nullptr || keys->length() <= r)
        {
            key = new MSequenceKey;
            key->advanceTimestep = false;
            key->transition = MCameraTransition::LINTERP;
        }
        else
        {
            key = keys->at(r);
        }

        sequence->keys.insert(position + r, key);
    }

    endInsertRows();

    return true;
}


bool MSequencerTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);

    beginRemoveRows(QModelIndex(), row, row + count - 1);

    for (int i = 0; i < count; i++)
    {
        MSequenceKey *key = sequence->keys[row + i];
        sequence->keys.removeAt(row + i);

        delete key; //Cleanup the key
    }

    endRemoveRows();

    return true;
}


bool MSequencerTableModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count,
                                    const QModelIndex &destinationParent, int destinationChild)
{
    if (sourceRow < 0 || sourceRow >= rowCount() || sourceRow + count - 1 < 0
            || sourceRow + count - 1 >= rowCount())
        return false;
    if (destinationChild < 0 || destinationChild >= rowCount() || destinationChild + count - 1 < 0
            || destinationChild + count - 1 >= rowCount())
        return false;

    beginMoveRows(sourceParent, sourceRow, sourceRow + count - 1, destinationParent, destinationChild);

    for (int i = 0; i < count; i++)
    {
        this->sequence->keys.move(sourceRow + i, destinationChild + i);
    }

    endMoveRows();

    return true;
}


QVariant MSequencerTableModel::data(const QModelIndex &index, int role = Qt::DisplayRole) const
{
    if (!sequence) return QVariant();

    // Check validity of requested index.
    if (!index.isValid() ||
            index.row() < 0 ||
            !(index.row() < this->sequence->keys.length()))
    {
        return QVariant();
    }

    MSequenceKey *key = sequence->keys.at(index.row());
    int column = index.column();

    if (role == Qt::DisplayRole)
    {
        switch (column)
        {
        case LON:
            return QVariant(key->position.x());
        case LAT:
            return QVariant(key->position.y());
        case Z:
            return QVariant(key->position.z());
        case PITCH:
            return QVariant(key->rotation.x());
        case YAW:
            return QVariant(key->rotation.y());
        case ROLL:
            return QVariant(key->rotation.z());
        case TELEPORT_TRANSITION:
            return QVariant();
        case NEXT_TIMESTEP:
            return QVariant();
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        return QVariant(int(Qt::AlignLeft | Qt::AlignCenter));
    }
    else if (role == Qt::CheckStateRole)
    {
        switch (column)
        {
        case TELEPORT_TRANSITION:
            return QVariant(key->transition == MCameraTransition::TELEPORT ? Qt::Checked : Qt::Unchecked);;
        case NEXT_TIMESTEP:
            return QVariant(static_cast<int>(key->advanceTimestep ? Qt::Checked : Qt::Unchecked));
        }
    }

    return QVariant();
}


bool MSequencerTableModel::setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole)
{
    Q_UNUSED(role);

    if (!sequence) return false;

    if (!index.isValid())
        return false;

    if (index.row() < 0 || index.row() >= sequence->keys.size())
        return false;

    MSequenceKey *key = sequence->keys.at(index.row());
    QModelIndex index2 = QModelIndex(index);

    bool success = true;
    int checked;

    switch (index.column())
    {
    case LON:
        key->position.setX(value.toFloat(&success));
        break;
    case LAT:
        key->position.setY(value.toFloat(&success));
        break;
    case Z:
        key->position.setZ(value.toFloat(&success));
        break;
    case PITCH:
        key->rotation.setX(value.toFloat(&success));
        break;
    case YAW:
        key->rotation.setY(value.toFloat(&success));
        break;
    case ROLL:
        key->rotation.setZ(value.toFloat(&success));
        break;
    case TELEPORT_TRANSITION:
        checked = value.toInt(&success);
        if (success)
        {
            if (checked == Qt::Checked)
            {
                key->transition = MCameraTransition::TELEPORT;
            }
            else
            {
                key->transition = MCameraTransition::LINTERP;
            }
        }
        break;
    case NEXT_TIMESTEP:
        checked = value.toInt(&success);
        if (success)
        {
            key->advanceTimestep = (checked == Qt::Checked);
        }
        break;
    }

    emit dataChanged(index, index2);

    return success;
}


Qt::ItemFlags MSequencerTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);

    if (!index.isValid())return flags;
    int column = index.column();

    // Editable columns
    if (column == LAT ||
            column == LON ||
            column == Z ||
            column == PITCH ||
            column == YAW ||
            column == ROLL)
    {
        return flags | Qt::ItemIsEditable;
    }

    if (column == NEXT_TIMESTEP ||
            column == TELEPORT_TRANSITION)
    {
        return flags | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
    }

    return flags;
}


void MSequencerTableModel::setSequence(MCameraSequence *sequence)
{
    if (sequence == this->sequence) return;

    if (this->sequence != nullptr)
    {
        beginRemoveRows(QModelIndex(), 0, this->rowCount() - 1);

        this->sequence = nullptr;

        endRemoveRows();
    }

    if (sequence == nullptr) return;

    beginInsertRows(QModelIndex(), 0, sequence->keys.length() - 1);

    this->sequence = sequence;

    endInsertRows();
}


MCameraSequence *MSequencerTableModel::getSequence() const
{
    return sequence;
}


MSequenceKey *MSequencerTableModel::getKeyForRow(int row) const
{
    if (row >= 0 && row < sequence->keys.count())
    {
        return sequence->keys[row];
    }

    return nullptr;
}


void MSequencerTableModel::updateRow(QModelIndex &row)
{
    emit dataChanged(row.siblingAtColumn(0), row.siblingAtColumn(COLUMN_COUNT - 1));
}


}
