/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mcamerasequencer.h"
#include "ui_mcamerasequencer.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "gxfw/camera.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/msystemcontrol.h"
#include "util/mutil.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MCameraSequencer::MCameraSequencer(QWidget *parent)
        : QWidget(parent),
          ui(new Ui::MCameraSequencer)
{
    ui->setupUi(this);
    ui->sequenceTable->horizontalHeader()->setStretchLastSection(true);
    ui->sequenceTable->setDragEnabled(true);
    ui->sequenceTable->setAcceptDrops(true);
    ui->sequenceTable->setDropIndicatorShown(true);
    ui->deleteButton->setEnabled(false);
    ui->sequenceComboBox->setEnabled(false);

    ui->runtimeLineEdit->setValidator(
            new QDoubleValidator(0.0, 999.0, 2, this));

    sequenceModel = new MSequencerTableModel();

    ui->sequenceTable->setModel(sequenceModel);
    ui->sequenceTable->resizeColumnsToContents();
    ui->sequenceTable->horizontalHeader()->setSectionResizeMode(
            QHeaderView::Stretch);

    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequenceRegistered, this,
            &MCameraSequencer::onSequenceRegistered);
    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequencesCleared, this,
            &MCameraSequencer::onSequencesCleared);
    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequenceRemoved, this,
            &MCameraSequencer::onSequenceRemoved);
    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequenceRenamed, this,
            &MCameraSequencer::onSequenceRenamed);
}


MCameraSequencer::~MCameraSequencer()
{ delete ui; }


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MCameraSequencer::addDefaultSequence()
{
    auto *defaultSeq = new MCameraSequence(false);

    defaultSeq->registerSequence();

    // Select newest sequence
    ui->sequenceComboBox->setCurrentIndex(ui->sequenceComboBox->count() - 1);
}


void MCameraSequencer::checkExistenceAndSave(QString filename)
{
    // Mostly adapted from MWaypointsView::checkExistanceAndSave(QString
    // filename)
    // TODO: (tv, 17May2023) Refactor these file operation methods, such as checkExistenceAndSave, which are used
    //       and needed multiple times, into a single MFileUtils class. Less duplicate code.
    //       This should be done in a single merge request, to reduce issues and review time.

    // Check whether the filename already exists.
    QRegExp filterlist(".*\\.(xml)");
    if (!filename.isEmpty())
    {
        if (!filename.endsWith(".xml", Qt::CaseInsensitive))
        {
            filename = filename.append(".xml");
        }

        QString filetype = QString(".xml");
        QFileInfo checkFile(filename);

        if (checkFile.exists())
        {
            // TODO (bt, 17Oct2016) Use operating system dependend filename
            // splitting.
            QMessageBox::StandardButton reply = QMessageBox::question(
                    this, "Save Camera Sequence",
                    filename.split("/").last() + " already exits.\n" +
                            "Do you want to replace it?",
                    QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

            if (reply == QMessageBox::No)
            {
                // Re-open FileDialog if file already exists and the user
                // chooses not to overwrite it or closes the question box.
                filename = QFileDialog::getSaveFileName(
                        this, "Save Camera Sequence", filename,
                        "Camera Sequence XML (*.xml)");

                // Quit if user closes file dialog.
                if (filename.isEmpty())
                    return;
            }
        }

        // Check if filename doesn't end with .xml
        // Repeat the test since the user could have changed the name in the
        // second FileDialog.
        while (!filterlist.exactMatch(filename))
        {
            // Append the selected file extension
            filename += filetype;
            // NOTE (bt, 17Oct2016): Can be removed if Qt-bug is fixed.
            // Need to check if file already exists since QFileDialog
            // doesn't provide this functionality under Linux compared to
            // https://bugreports.qt.io/browse/QTBUG-11352 .
            QFileInfo checkFile(filename);
            if (checkFile.exists())
            {
                // TODO (bt, 17Oct2016) Use operating system dependend filename
                // splitting.
                QMessageBox::StandardButton reply = QMessageBox::question(
                        this, "Save Camera Sequence",
                        filename.split("/").last() + " already exits.\n" +
                                "Do you want to replace it?",
                        QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

                if (reply == QMessageBox::No)
                {
                    // Re-open FileDialog if file already exists and the user
                    // chooses not to overwrite it or closes the question box.
                    filename = QFileDialog::getSaveFileName(
                            this, "Save Camera Sequence", filename,
                            "Camera Sequence XML (*.xml)");
                    // Quit if user closes file dialog.
                    if (filename.isEmpty())
                        return;
                }
            }
        }

        this->sequenceModel->getSequence()->saveToFile(filename);
    }

    return;
}


/******************************************************************************
***                           PRIVATE SLOTS                                 ***
*******************************************************************************/

void MCameraSequencer::openSequence()
{
    QString dir = MSystemManagerAndControl::getInstance()->getMet3DWorkingDirectory().path();
    QString fileName = QFileDialog::getOpenFileName(
            this, "Open Camera Sequence", dir, "Camera Sequence XML (*.xml)");
    if (!fileName.isEmpty())
    {
        auto *seq = new MCameraSequence(false);
        seq->loadFromFile(fileName);
        seq->registerSequence();

        ui->sequenceComboBox->setCurrentIndex(ui->sequenceComboBox->count() - 1);
    }
}


void MCameraSequencer::saveSequence()
{
    if (this->sequenceModel->getSequence() == nullptr)
    {
        QMessageBox::warning(this, "No camera sequence found", "No camera sequence was found that can be saved.");
        return;
    }

    if (this->sequenceModel->getSequence()->getFileName() == nullptr)
    {
        saveAsSequence();
    }
    else
    {
        checkExistenceAndSave(
                this->sequenceModel->getSequence()->getFileName());
    }
}


void MCameraSequencer::saveAsSequence()
{
    if (this->sequenceModel->getSequence() == nullptr)
    {
        QMessageBox::warning(this, "No camera sequence found", "No camera sequence was found that can be saved.");
        return;
    }

    QString dir = MSystemManagerAndControl::getInstance()->getMet3DWorkingDirectory().path();

    QString fileName = QFileDialog::getSaveFileName(
            this, "Save Camera Sequence", dir, "Camera Sequenec XML (*.xml)");

    if (fileName.length() > 0)
    {
        checkExistenceAndSave(fileName);
    }
}


void MCameraSequencer::addNewKey()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    // When invalid, add as last
    int row = currentIndex.isValid() ? currentIndex.row() + 1
                                     : sequenceModel->rowCount();

    if (this->sequenceModel->getSequence() == nullptr)
    {
        addDefaultSequence();
    }

    this->sequenceModel->insertRows(row, 1, currentIndex, nullptr);
}


void MCameraSequencer::addNewKeyFromCamera()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    // When invalid, add as last
    int row = currentIndex.isValid() ? currentIndex.row() + 1
                                     : sequenceModel->rowCount();

    if (this->sequenceModel->getSequence() == nullptr)
    {
        addDefaultSequence();
    }

    auto *key = new MSequenceKey();
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MSceneViewGLWidget *sceneView = sysMC->getLastInteractedSceneView();

    if (sceneView)
    {
        MCamera *camera = sceneView->getCamera();

        if (camera)
        {
            key->position = camera->getOrigin();
            key->rotation = QVector3D(camera->getPitch(), camera->getYaw(),
                                      camera->getRoll());
            key->transition = MCameraTransition::LINTERP;
        }
    }

    QList<MSequenceKey *> keys;
    keys.append(key);

    this->sequenceModel->insertRows(row, 1, currentIndex, &keys);
}


void MCameraSequencer::setKeyFromCamera()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    int row = currentIndex.isValid() ? currentIndex.row() : 0;

    if (this->sequenceModel->getSequence() == nullptr)
    {
        addDefaultSequence();
    }

    MSequenceKey *key = this->sequenceModel->getKeyForRow(row);

    if (key == nullptr) return;

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MSceneViewGLWidget *sceneView = sysMC->getLastInteractedSceneView();

    if (sceneView)
    {
        MCamera *camera = sceneView->getCamera();

        if (camera)
        {
            key->position = camera->getOrigin();
            key->rotation = QVector3D(camera->getPitch(), camera->getYaw(),
                                      camera->getRoll());
        }
    }

    QList<MSequenceKey *> keys;
    keys.append(key);

    this->sequenceModel->updateRow(currentIndex);
}


void MCameraSequencer::viewKeyInCamera()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid())
        return;

    int row = currentIndex.row();

    MSequenceKey *key = sequenceModel->getKeyForRow(row);

    if (!key)
        return;

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MSceneViewGLWidget *sceneView = sysMC->getLastInteractedSceneView();

    if (sceneView)
    {
        MCamera *camera = sceneView->getCamera();

        if (camera)
        {
            camera->setOrigin(key->position);

            QVector3D rot = key->rotation;
            camera->setRotation(rot.x(), rot.y(), rot.z());

            sceneView->updateCamera();
        }
    }
}


void MCameraSequencer::removeKey()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid())
        return;

    int row = currentIndex.row();

    if (!sequenceModel)
        return;

    this->sequenceModel->removeRows(row, 1, currentIndex);
}


void MCameraSequencer::removeSequence()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    sysMC->removeCameraSequence(ui->sequenceComboBox->currentIndex());
}


void MCameraSequencer::loopChecked(int state)
{
    if (sequenceModel->getSequence() == nullptr) return;

    if (state > 0)
    {
        sequenceModel->getSequence()->loopSequence = true;
    }
    else
    {
        sequenceModel->getSequence()->loopSequence = false;
    }
}


void MCameraSequencer::sequenceChanged(int index)
{
    if (index < 0)
    {
        this->sequenceModel->setSequence(nullptr);
        ui->sequenceComboBox->setCurrentIndex(index);
        ui->runtimeLineEdit->setText(QString::number(10));
        ui->loopCheckbox->setChecked(false);
        return;
    }

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    MCameraSequence *sequence = sysMC->getCameraSequence(index);

    if (sequence == nullptr)
    {
        // Refresh list of sequences
        onSequenceRegistered();
        sequence = sysMC->getCameraSequence(0);
        ui->sequenceComboBox->setCurrentIndex(0);
        return;
    }

    this->sequenceModel->setSequence(sequence);
    ui->runtimeLineEdit->setText(
            QString::number(sequenceModel->getSequence()->sequenceRuntime));
    ui->loopCheckbox->setChecked(sequenceModel->getSequence()->loopSequence);
}


void MCameraSequencer::runtimeChanged(QString text)
{
    bool ok = false;
    double value = text.toDouble(&ok);

    if (ok)
    {
        auto sequence = sequenceModel->getSequence();
        if (sequence)
            sequence->sequenceRuntime = value;
    }
}


void MCameraSequencer::moveKeyUp()
{
    if (this->sequenceModel->getSequence() == nullptr) return;

    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid()) return;

    int row = currentIndex.row();

    // Cant move up if we are in the first row.
    if (row <= 0) return;

    this->sequenceModel->moveRows(currentIndex.parent(), row, 1, currentIndex.parent(), row - 1);
}


void MCameraSequencer::moveKeyDown()
{
    if (this->sequenceModel->getSequence() == nullptr) return;

    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid()) return;

    int row = currentIndex.row();

    // Cant move down if we are in the last row.
    if (row >= this->sequenceModel->rowCount() - 1) return;

    this->sequenceModel->moveRows(currentIndex.parent(), row + 1, 1, currentIndex.parent(), row);
}


void MCameraSequencer::onSequenceRegistered()
{
    int selected = ui->sequenceComboBox->currentIndex();

    ui->sequenceComboBox->clear();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    auto sequences = sysMC->getCameraSequences();

    for (int i = 0; i < sequences.length(); i++)
    {
        QString label = sequences.at(i)->sequenceName;
        ui->sequenceComboBox->addItem(label, QVariant(i));
    }

    int count = ui->sequenceComboBox->count();

    if (selected >= count)
    {
        selected = count - 1;
    }
    if (selected < 0)
    {
        selected = 0;
    }

    ui->deleteButton->setEnabled(count > 0);
    ui->sequenceComboBox->setEnabled(count > 0);

    ui->sequenceComboBox->setCurrentIndex(selected);
}


void MCameraSequencer::onSequencesCleared()
{
    ui->sequenceComboBox->clear();
    ui->sequenceComboBox->setCurrentIndex(-1);
    this->sequenceModel->setSequence(nullptr);
    ui->runtimeLineEdit->setText(QString::number(10));
    ui->loopCheckbox->setChecked(false);
    ui->deleteButton->setEnabled(false);
}


void MCameraSequencer::onSequenceRemoved(int index)
{
    int selected = ui->sequenceComboBox->currentIndex();

    if (selected < 0)
        return;

    ui->sequenceComboBox->clear();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    auto sequences = sysMC->getCameraSequences();

    for (int i = 0; i < sequences.length(); i++)
    {
        QString label = sequences.at(i)->sequenceName;
        ui->sequenceComboBox->addItem(label, QVariant(i));
    }

    int count = ui->sequenceComboBox->count();

    if (selected > index)
    {
        selected--;
    }
    else if (selected == index)
    {
        selected = count > 0 ? 0 : -1;
    }

    ui->deleteButton->setEnabled(count > 0);
    ui->sequenceComboBox->setEnabled(count > 0);

    ui->sequenceComboBox->setCurrentIndex(selected);
}


void MCameraSequencer::onRenameSequence(QString newName)
{
    int index = ui->sequenceComboBox->currentIndex();
    if (index < 0)
        return;

    if (this->sequenceModel->getSequence() == nullptr)
        return;

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    sysMC->renameCameraSequence(ui->sequenceComboBox->currentIndex(), newName);
}


void MCameraSequencer::onSequenceRenamed(int index, QString name)
{
    ui->sequenceComboBox->setItemText(index, name);
}


void MCameraSequencer::onCreateCameraSequence()
{ addDefaultSequence(); }


} // namespace Met3D
