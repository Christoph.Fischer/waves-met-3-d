/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSHAREDCONSTANTS_H
#define MSHAREDCONSTANTS_H

/**
 * This file is a shared header for both, C++ and GLSL code.
 * This file can be included in both.
 * It will contain constants, that should be shared between both
 * sides of the code, to make updates to those constants easier,
 * and ease the use of those constants by other developers.
 */

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/
/**
 * This value is a constant value in place for missing values in datasets.
 * It is -infinity, so that hardware interpolation will also result in -infinity, if any of the
 * interpolated values is M_MISSING_VALUE. The only exceptions is -infinity / infinity which results in NaN.
 * This will only work in glsl versions 414 and higher.
 */
const float M_MISSING_VALUE = -1.0 / 0.0;
// For historical reasons some shaders use MISSING_VALUE instead of M_MISSING_VALUE, so just define another constant.
#define MISSING_VALUE M_MISSING_VALUE

#endif // MSHAREDCONSTANTS_H
